<?php
/**
 * WEB4PRO - Creating profitable online stores
 *
 * @author WEB4PRO <ppanchyshnyy@corp.web4pro.com.ua>
 * @category  WEB4PRO
 * @package   Web4pro_Hintsdebug
 * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
 * @license   http://www.web4pro.net/license.txt
 */

class Web4pro_Hintsdebug_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Checking allowed IPs for debugging
     *
     * @return int
     */
    public function checkAllowsIps()
    {
        $dem = 0;
        $allowedIps = Mage::getStoreConfig('dev/restrict/allow_ips');

        if(empty($allowedIps)) return 1;

        $remote = Mage::helper('core/http')->getRemoteAddr();
        $allowedIps = explode(",",$allowedIps);

        foreach ($allowedIps as $value) {
            if(trim($remote) == trim($value))
            {
                $dem = 1;
                break;
            }
        }
        return $dem;
    }
}