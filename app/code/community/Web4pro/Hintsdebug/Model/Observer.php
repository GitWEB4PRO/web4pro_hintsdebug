<?php
/**
 * WEB4PRO - Creating profitable online stores
 *
 * @author WEB4PRO <ppanchyshnyy@corp.web4pro.com.ua>
 * @category  WEB4PRO
 * @package   Web4pro_Hintsdebug
 * @copyright Copyright (c) 2015 WEB4PRO (http://www.web4pro.net)
 * @license   http://www.web4pro.net/license.txt
 */

class Web4pro_Hintsdebug_Model_Observer
{
    const TEMPLATE_HINTS        = 'dev/debug/template_hints';
    const TEMPLATE_HINTS_BLOCKS = 'dev/debug/template_hints_blocks';
    const DEBUG_PARAM           = 'web4pro_hintsdebug/general/get_param';
    const ENABLE_DEBUG          = 'web4pro_hintsdebug/general/enable';

    /**
     * Enabling or disabling template hints on frontend
     *
     * @param Varien_Event_Observer $observer
     * @event controller_action_predispatch
     */
    public function templateHintsHandler(Varien_Event_Observer $observer)
    {
        $store   = Mage::app()->getStore();
        $storeId = $store->getId();
        if(!Mage::getStoreConfig(self::ENABLE_DEBUG, $storeId)){
            return;
        }

        $helper  = Mage::helper('web4pro_hintsdebug');
        $request = $observer->getControllerAction()->getRequest();
        $scopeId = $store->getWebsiteId();

        $websiteTemplateHints = Mage::getStoreConfigFlag(self::TEMPLATE_HINTS, $scopeId);
        $websiteTemplateHintsBlocks = Mage::getStoreConfigFlag(self::TEMPLATE_HINTS_BLOCKS, $scopeId);

        $storeTemplateHints = Mage::getStoreConfigFlag(self::TEMPLATE_HINTS, $scopeId);
        $storeTemplateHintsBlocks = Mage::getStoreConfigFlag(self::TEMPLATE_HINTS_BLOCKS, $scopeId);

        if($helper->checkAllowsIps() > 0){
            $param   = Mage::getStoreConfig(self::DEBUG_PARAM, $storeId);
            $enabled = $request->getParam($param);

            if(isset($enabled)){
                $enabled = 1;

                if($websiteTemplateHints && $websiteTemplateHintsBlocks){
                    return;
                }
                if(!$storeTemplateHints || !$storeTemplateHintsBlocks){
                    $this->_saveConfiguration($enabled, 'stores', $storeId);
                }

                $this->_saveConfiguration($enabled, 'websites', $storeId);
                $store->resetConfig();
            }elseif(($websiteTemplateHints && $websiteTemplateHintsBlocks) || ($storeTemplateHints && $storeTemplateHintsBlocks)){
                $enabled = 0;

                $this->_saveConfiguration($enabled, 'websites', $storeId);
                $this->_saveConfiguration($enabled, 'stores', $storeId);
                $store->resetConfig();
            }
        }
    }

    /**
     * Saves configuration to DB
     *
     * @param $value
     * @param $scope
     * @param $scope_id
     */
    protected function _saveConfiguration($value, $scope, $scope_id)
    {
        Mage::getConfig()->saveConfig(self::TEMPLATE_HINTS, $value, $scope, $scope_id);
        Mage::getConfig()->saveConfig(self::TEMPLATE_HINTS_BLOCKS, $value, $scope, $scope_id);
    }
}